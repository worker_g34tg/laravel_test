<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTamagotchiTableWithCriticalHungryTime extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tamagotchis', function (Blueprint $table) {
            $table->timestamp('starve_begin')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tamagotchis', function (Blueprint $table) {
            $table->dropColumn('starve_begin');
        });
    }
}
