<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddingLastActionFieldsToTamagotchiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tamagotchis', function (Blueprint $table) {
            $table->timestamp('last_feed')->nullable();
            $table->timestamp('last_sleep')->nullable();
            $table->timestamp('last_care')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tamagotchis', function (Blueprint $table) {
            $table->dropColumn('last_care');
            $table->dropColumn('last_sleep');
            $table->dropColumn('last_feed');
        });
    }
}
