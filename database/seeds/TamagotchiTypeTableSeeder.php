<?php

use Illuminate\Database\Seeder;

class TamagotchiTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Четыре типа тамагочи
        $tamagochie_type = new \App\TamagotchiType();
        $tamagochie_type->name = 'Cat';
        $tamagochie_type->icon = 'cat.png';
        $tamagochie_type->save();

        $tamagochie_type_2 = new \App\TamagotchiType();
        $tamagochie_type_2->name = 'Dog';
        $tamagochie_type_2->icon = 'dog.png';
        $tamagochie_type_2->save();

        $tamagochie_type_3 = new \App\TamagotchiType();
        $tamagochie_type_3->name = 'Raccoon';
        $tamagochie_type_3->icon = 'raccoon.png';
        $tamagochie_type_3->save();

        $tamagochie_type_4 = new \App\TamagotchiType();
        $tamagochie_type_4->name = 'Penguin';
        $tamagochie_type_4->icon = 'penguin.png';
        $tamagochie_type_4->save();

        $first_tamagotchi = new \App\Tamagotchi();
        $first_tamagotchi->users_id = 1;
        $first_tamagotchi->type_id = rand(1, 4);
        $first_tamagotchi->fullness = 100;
        $first_tamagotchi->sleep = 100;
        $first_tamagotchi->care = 100;
        $first_tamagotchi->status = \App\Tamagotchi::STATUS_LIVE;
        $first_tamagotchi->save();
    }
}
