<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // один юзер для тестирования
        $user = new \App\User();
        $user->name = 'User 1';
        $user->email = 'user.mail@example.com';
        $user->password = bcrypt('123456');
        $user->save();
    }
}
