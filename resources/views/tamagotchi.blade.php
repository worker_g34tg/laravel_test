@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h4> Your tamagotchi {{$tamagotchi->type->name}}</h4>

                    <div class="col-lg-12">
                        <p>
                            <a href="/home"><button> Back to others</button></a>
                        </p>
                    </div>

                    <div class="col-lg-12">
                        <div class="thumbnail">
                            <div class="caption" ref="tamagotchiId" data-tamagotchi-id="{{ $tamagotchi->id }}">
                                <h3> {{ $tamagotchi->type->name }}</h3>
                                <img class="thumbnail" style="width: 300px;" src="/img/{{ $tamagotchi->type->icon }}" alt="">

                                <div v-if="tamagotchiIsDead === true" class="alert alert-danger">
                                    <ul>
                                        <li>Your tamagotchi is dead. You are lose :-(</li>
                                    </ul>
                                </div>

                                <div v-if="someError === true" class="alert alert-danger">
                                    <ul>
                                        <li><span v-html="tamagotchi.message">{{ $tamagotchi->message }}</span></li>
                                    </ul>
                                </div>

                                <table class="table">
                                    <input type="hidden">
                                    <tbody>
                                    <tr>
                                        <td>Fullness : <span v-html="tamagotchi.fullness">{{$tamagotchi->fullness}}</span></td>
                                        <td><button v-on:click="toFeed({{$tamagotchi}})" id="to_feed" data-tamagochi-id="{{$tamagotchi->id}}"> To feed</button></td>
                                    </tr>
                                    <tr>
                                        <td>Sleep : <span v-html="tamagotchi.sleep">{{ $tamagotchi->sleep }}</span></td>
                                        <td><button v-on:click="toMakeSleep({{$tamagotchi}})" id="to_sleep"> To make sleep</button></td>
                                    </tr>
                                    <tr>
                                        <td>Care : <span v-html="tamagotchi.care">{{ $tamagotchi->care }}</span> </td>
                                        <td><button v-on:click="toCare({{$tamagotchi}})" id="to_care"> To care</button></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
