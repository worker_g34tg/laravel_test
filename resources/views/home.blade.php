@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-lg-12">
                            <h4> Your tamagotchies </h4>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">

                            @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif

                            <div class="thumbnail">
                                <form action="/home/add-tamagotchi" method="post">
                                    <h5>Add another one tamagotchi</h5>
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <label for="tamagotchi-type-id">Choose tamagotchi type</label>
                                        <select name="tamagotchi_type_id" id="tamagotchi-type-id">
                                            @foreach($tamagotchi_types as $type)
                                                <option value="{{$type->id}}" @if( in_array($type->id, $tamagotchi_types_user_have) )
                                                                      {{'disabled'}}
                                                                  @endif
                                                >{{$type->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <input type="hidden" name="user_id" value="{{Auth::user()->id}}">

                                    <input type="submit" value="Add tamagotchi">

                                </form>
                                <br>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row">
                                @foreach($tamagotchies as $tamagotchi)
                                    <div class="col-lg-6">
                                        <div class="thumbnail">
                                            <div class="caption" ref="tamagotchiId" data-tamagotchi-id="{{ $tamagotchi->id }}">
                                                <a href="/home/tamagotchi/{{$tamagotchi->id}}">
                                                    <h3> {{ $tamagotchi->type->name }}</h3>
                                                    <img class="thumbnail" style="width: 100px;" src="/img/{{ $tamagotchi->type->icon }}" alt="">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
