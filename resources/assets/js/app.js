
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

const app = new Vue({
    el: '#app',

    data:{
        tamagotchi: [],
        tamagotchiIsDead: false,
        someError: false
    },

    mounted: function () {

        var tamagotchiId = this.$refs ? this.$refs.tamagotchiId.dataset.tamagotchiId : null;

        this.getTamagotchiData( tamagotchiId );

        Echo.private('myChannel.'+tamagotchiId)
            .listen('TamagotchiParamsUpdated', (e) => {
                this.someError = false;
                this.tamagotchi = e.tamagotchi;
            });

        Echo.private('myChannel.'+tamagotchiId)
            .listen('TamagotchiDead', (e) => {
            this.someError = false;
                this.tamagotchiIsDead = true;
            });
    },

    methods: {

        getTamagotchiData: function ( tamagotchiId ) {

            axios.get('/api/getTamagotchi/' + tamagotchiId)
                .then(response => {
                    this.tamagotchi = response.data.data;

                    if (this.tamagotchi.status == 0) {
                        this.tamagotchiIsDead = true;
                    }
                })
        },

        toFeed: function ( object ) {

            this.someError = false;

            axios.put('/api/feed/' + object.id)
                .then(response => {
                    this.tamagotchi = response.data.data;
                    this.checkSomeError( this );
                }).catch (
                        (error) => console.log( error )
                );
        },

        toMakeSleep: function (object) {

            this.someError = false;

            axios.put('/api/sleep/' + object.id)
                .then(response => {
                    this.tamagotchi = response.data.data;
                    this.checkSomeError( this );
                }).catch (
                        (error) => console.log( error )
                );
        },

        toCare: function (object) {

            this.someError = false;

            axios.put('/api/care/' + object.id)
                .then(response => {
                    this.tamagotchi = response.data.data;
                    this.checkSomeError( this );
                }).catch (
                        (error) => console.log( error )
                );
        },

        checkSomeError: function (object) {
            if ( object.tamagotchi.message !== null ) {
                object.someError = true;
            }
        }
    }

});
