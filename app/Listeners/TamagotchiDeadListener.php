<?php

namespace App\Listeners;

use App\Events\TamagotchiDead;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class TamagotchiDeadListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TamagotchiDead  $event
     * @return void
     */
    public function handle(TamagotchiDead $event)
    {
        //
    }
}
