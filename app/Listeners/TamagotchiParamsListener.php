<?php

namespace App\Listeners;

use App\Events\TamagotchiParamsUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class TamagotchiParamsListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TamagotchiParamsUpdated  $event
     * @return void
     */
    public function handle(TamagotchiParamsUpdated $event)
    {
        //
    }
}
