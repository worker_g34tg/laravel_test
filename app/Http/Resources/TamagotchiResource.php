<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class TamagotchiResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->resource->id,
            'care' => $this->resource->care,
            'sleep' => $this->resource->sleep,
            'fullness' => $this->resource->fullness,
            'status' => $this->resource->status,
            'message' => $this->resource->message,
        ];
    }
}
