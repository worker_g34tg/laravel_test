<?php

namespace App\Http\Controllers;

use App\Http\Resources\TamagotchiResource;
use App\Tamagotchi;
use Illuminate\Http\Request;

class NeedsController extends Controller
{
    /**
     * Кормление
     * @param Request $request
     * @param $id
     * @return TamagotchiResource|\Symfony\Component\HttpFoundation\Response
     */
    public function feed( Request $request, $id )
    {
        $tamagotchi = Tamagotchi::getTamagotchiByIdIfAlive( $id );

        if ( $tamagotchi ) {

            if ($tamagotchi->fullness < Tamagotchi::MAX_FULLNESS && $tamagotchi->canBeFeed() ) {
                $tamagotchi->fullness += 1;
                $tamagotchi->last_feed = now();
                $tamagotchi->starve_begin = null;
                $tamagotchi->save();
            }else{
                $tamagotchi->message = 'You can not feed Tamagotchi more often than once per 5 minutes';
            }

            return TamagotchiResource::make($tamagotchi);
        }else{
            return response(['message' => 'There is no tamagothci model with id "'.$id.'", or this tamagotchi is dead'], 500);
        }
    }

    /**
     * Удовлетворение потребности во сне
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function sleep( Request $request, $id )
    {
        $tamagotchi = Tamagotchi::getTamagotchiByIdIfAlive( $id );

        if ( $tamagotchi ) {

            if ( $tamagotchi->sleep < Tamagotchi::MAX_SLEEP && $tamagotchi->canBeSleep() ) {
                $tamagotchi->sleep += 1;
                $tamagotchi->last_sleep = now();
                $tamagotchi->save();
            }else{
                $tamagotchi->message = 'You can not make Tamagotchi sleep more often than once per 10 minutes';
            }

            return TamagotchiResource::make($tamagotchi);
        }else{
            return response(['message' => 'There is no tamagothci model with id "'.$id.'", or this tamagotchi is dead'], 500);
        }
    }

    /**
     * Удовлетворение заботы
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function care( Request $request, $id )
    {
        $tamagotchi = Tamagotchi::getTamagotchiByIdIfAlive( $id );

        if ( $tamagotchi ) {

            if ( $tamagotchi->care < Tamagotchi::MAX_CARE && $tamagotchi->canBeCare() ) {
                $tamagotchi->care += 1;
                $tamagotchi->last_care = now();
                $tamagotchi->save();
            }else{
                $tamagotchi->message = 'You can not make care more often than once per 5 minutes';
            }

            return TamagotchiResource::make($tamagotchi);
        }else{
            return response(['message' => 'There is no tamagothci model with id "'.$id.'", or this tamagotchi is dead'], 500);
        }
    }

    /**
     * Найти конкретного тамагочи
     * @param Request $request
     * @param $tamagotchi_id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getTamagotchi( Request $request, $tamagotchi_id)
    {
        $tamagotchi = Tamagotchi::find( $tamagotchi_id );

        if ( $tamagotchi ) {
            return TamagotchiResource::make($tamagotchi);
        }else{
            return response(['message' => 'There is no tamagothci model with id "'.$tamagotchi_id.'", or required param id is missing'], 500);
        }
    }
}
