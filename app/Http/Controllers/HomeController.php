<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddNewTamagotchi;
use App\Tamagotchi;
use App\TamagotchiType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tamagotchies = Auth::user()->tamagotchies;
        $tamagotchi_types = TamagotchiType::all();
        $tamagotchi_types_user_have = Auth::user()->tamagotchiesUserAlreadyHave();

        return view('home', [
            'tamagotchies' => $tamagotchies,
            'tamagotchi_types' => $tamagotchi_types,
            'tamagotchi_types_user_have' => $tamagotchi_types_user_have,
        ]);
    }

    /**
     * Отобразить тамагочи
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function tamagotchi( Request $request, $id)
    {
        $tamagotchi = Tamagotchi::find( $id );

        return view('tamagotchi', ['tamagotchi' => $tamagotchi]);
    }

    /**
     * Пользователь добавляет нового тамагочи
     * @param AddNewTamagotchi $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function addTamagotchi(AddNewTamagotchi $request)
    {
        if ( $validated = $request->validated() ) {

            $new_tamagothci = new Tamagotchi();
            $new_tamagothci->addNewTamagothci($request);
        }

        return redirect('/home');
    }
}
