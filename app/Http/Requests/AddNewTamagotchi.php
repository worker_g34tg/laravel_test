<?php

namespace App\Http\Requests;

use App\Tamagotchi;
use Illuminate\Foundation\Http\FormRequest;

class AddNewTamagotchi extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tamagotchi_type_id' => 'required',
            'user_id' => 'required',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {

            $user_id = $this->post('user_id');
            $tamagotchi_type_id = $this->post('tamagotchi_type_id');

            if ( Tamagotchi::checkUserHaveTamagotchi($user_id, $tamagotchi_type_id)) {
                $validator->errors()->add('tamagotchi_type_id', 'You can have only one tamagotchi of each type!');
            }
        });
    }
}
