<?php

namespace App\Console\Commands;

use App\Events\TamagotchiDead;
use App\Tamagotchi;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CheckIsDead extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CheckIsDead:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check for tamagotchies is dead by hunger';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tamagotchies = Tamagotchi::where([
                ['fullness', '<', 5],
                ['status', '=', Tamagotchi::STATUS_LIVE],
                ['starve_begin', '<=', now()->subHour()]
            ])->get();

        foreach ($tamagotchies as $tamagotchi) {

            $tamagotchi->status = Tamagotchi::STATUS_DEAD;
            $tamagotchi->save();

            event( new TamagotchiDead( $tamagotchi ) );
        }

    }
}
