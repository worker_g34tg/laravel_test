<?php

namespace App\Console\Commands;

use App\Events\TamagotchiParamsUpdated;
use App\Tamagotchi;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class FastReduceCare extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'FastReduceCare:reduce';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This care reducing when sleep lower than 5';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tamagotchies = Tamagotchi::where([
                ['status', '=', Tamagotchi::STATUS_LIVE],
                ['care', '>', 0],
                ['sleep', '<', 5]
            ])->get();

        foreach ($tamagotchies as $tamagotchi) {

            $tamagotchi->care--;
            $tamagotchi->save();

            event( new TamagotchiParamsUpdated($tamagotchi) );
        }
    }
}
