<?php

namespace App\Console\Commands;

use App\Events\TamagotchiParamsUpdated;
use App\Tamagotchi;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class RiseHungry extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'RiseHungry:rise';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reduce fullness value for tamagotchies';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tamagotchies = Tamagotchi::where([
            ['status', '=', Tamagotchi::STATUS_LIVE],
            ['fullness', '>', 0]
        ])->get();

        foreach ($tamagotchies as $tamagotchi) {

            $tamagotchi->fullness--;

            if ( $tamagotchi->fullness < 5 && $tamagotchi->starve_begin == null ) {
                $tamagotchi->starve_begin = now();
            }

            $tamagotchi->save();

            event( new TamagotchiParamsUpdated($tamagotchi) );
        }
    }
}
