<?php

namespace App\Console\Commands;

use App\Events\TamagotchiParamsUpdated;
use App\Tamagotchi;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ReduceSleep extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ReduceSleep:reduce';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reduce sleep value of tamagotchies';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tamagotchies = Tamagotchi::where([
                ['status', '=', Tamagotchi::STATUS_LIVE],
                ['sleep', '>', 0]
            ])->get();

        foreach ($tamagotchies as $tamagotchi) {
            $tamagotchi->sleep--;
            $tamagotchi->save();

            event( new TamagotchiParamsUpdated($tamagotchi) );
        }
    }
}
