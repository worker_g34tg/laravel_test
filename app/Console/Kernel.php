<?php

namespace App\Console;

use App\Console\Commands\CheckIsDead;
use App\Console\Commands\FastReduceCare;
use App\Console\Commands\ReduceCare;
use App\Console\Commands\ReduceSleep;
use App\Console\Commands\RiseHungry;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command(CheckIsDead::class)->everyMinute();
        $schedule->command(FastReduceCare::class)->everyFiveMinutes();
        $schedule->command(RiseHungry::class)->everyTenMinutes();
        $schedule->command(ReduceCare::class)->everyFifteenMinutes();
        $schedule->command(ReduceSleep::class)->cron('*/20 * * * * *');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
