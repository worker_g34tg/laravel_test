<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the tamagochies
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tamagotchies()
    {
        return $this->hasMany('App\Tamagotchi', 'users_id');
    }

    public function tamagotchiesUserAlreadyHave()
    {
        $tamagotchies = $this->tamagotchies;

        $array = [];

        foreach ($tamagotchies as $t) {
            $array[] = $t->type->id;
        }

        return $array;
    }
}
