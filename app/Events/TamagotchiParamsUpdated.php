<?php

namespace App\Events;

use App\Tamagotchi;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class TamagotchiParamsUpdated implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $tamagotchi;

    /**
     * Create a new event instance.
     *
     * @param $tamagotchi Tamagotchi
     * @return void
     */
    public function __construct($tamagotchi)
    {
        $this->tamagotchi = $tamagotchi;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('myChannel.'.$this->tamagotchi->id);
    }
}
