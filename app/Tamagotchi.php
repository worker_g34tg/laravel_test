<?php

namespace App;

use App\Http\Requests\AddNewTamagotchi;
use Illuminate\Database\Eloquent\Model;

class Tamagotchi extends Model
{
    /** Tamagotchi needs maximum values */
    const MAX_FULLNESS = 100;
    const MAX_SLEEP = 100;
    const MAX_CARE = 100;

    /** Tamagotchi life state */
    const STATUS_DEAD = 0;
    const STATUS_LIVE = 1;

    /** @var array $dates */
    protected $dates = [
        'last_care', 'last_sleep', 'last_feed',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Users', 'users_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo('App\TamagotchiType', 'type_id');
    }

    /**
     *
     * @param $user_id
     * @param $tamagotchi_type_id
     * @return mixed
     */
    public static function checkUserHaveTamagotchi($user_id, $tamagotchi_type_id)
    {
        return self::where(['users_id' => $user_id, 'type_id' => $tamagotchi_type_id])->first();
    }

    /**
     * Добавить нового тамагочи для пользователя
     * @param AddNewTamagotchi $request
     * @return bool
     */
    public function addNewTamagothci( AddNewTamagotchi $request)
    {
        $this->users_id = $request->user_id;
        $this->type_id  = $request->tamagotchi_type_id;
        $this->status   = self::STATUS_LIVE;
        $this->fullness = self::MAX_FULLNESS;
        $this->sleep    = self::MAX_SLEEP;
        $this->care     = self::MAX_CARE;

        return $this->save();
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function getTamagotchiByIdIfAlive( $id )
    {
        return self::where([
            ['id', '=',  $id],
            ['status', '=', self::STATUS_LIVE]
        ])->first();
    }

    /**
     * Может быть покормлен (раз в 5 минут)
     * @return bool
     */
    public function canBeFeed()
    {
        return $this->last_feed < now()->subMinutes(5);
    }

    /**
     * Может поспать
     * @return bool
     */
    public function canBeSleep()
    {
        return $this->last_sleep < now()->subMinutes(10);
    }

    /**
     * Можно применить заботу
     * @return bool
     */
    public function canBeCare()
    {
        return $this->last_care < now()->subMinutes(5);
    }
}
