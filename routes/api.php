<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::put('/feed/{id}', [
    'uses' => 'NeedsController@feed'
]);

Route::put('/sleep/{id}', [
    'uses' => 'NeedsController@sleep'
]);

Route::put('/care/{id}', [
    'uses' => 'NeedsController@care'
]);

Route::get('/getTamagotchi/{tamagotchi_id}', [
    'uses' => 'NeedsController@getTamagotchi'
]);